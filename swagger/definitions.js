const lib = require('./lib');
const base = require('./base');
const id = base.id;

const title = lib.property('string', 'Название');
const date = base.date('Срок выполнения');

module.exports = {
    SaveResponse: {
        type: 'object',
        properties: {id}
    },
    Task: {
        type: 'object',
        properties: {
            id,
            title,
            date,
            isDone: lib.property('boolean', 'Завершена ли задача')
        }
    },
    TaskSaving: {
        type: 'object',
        properties: {
            title,
            date
        }
    }
};
