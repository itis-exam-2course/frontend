const lib = require('./lib');
const definitions = require('./definitions');

module.exports = {
    '/tasks': {
        get: {
            tags: ['tasks'],
            summary: 'Получить список задач',
            parameters: [
                lib.parameter('query', 'search', 'string', {
                    description: 'Поиск по названию задачи'
                })
            ],
            responses: {
                200: lib.response('Список задач', {
                    type: 'array',
                    items: definitions.Task
                })
            }
        },
        post: {
            summary: 'Создать задачу',
            tags: ['tasks'],
            parameters: [lib.postBody('TaskSaving')],
            responses: {
                200: lib.response('Задача сохранена', definitions.SaveResponse),
                400: lib.response('Неправильные параметры запроса')
            }
        }
    },
    '/tasks/{id}': {
        get: {
            summary: 'Получить актив',
            tags: ['tasks'],
            parameters: [
                lib.parameter('path', 'id', 'integer', {
                    description: 'ID актива',
                    format: 'uuid',
                    required: true
                })
            ],
            responses: {
                200: lib.response('Информация о задаче', definitions.Task),
                404: lib.response('Задача не найдена')
            }
        },
        put: {
            summary: 'Сохранить задачу',
            tags: ['tasks'],
            parameters: [
                lib.parameter('path', 'id', 'integer', {
                    description: 'ID задачи',
                    required: true
                }),
                lib.postBody('TaskSaving')
            ],
            responses: {
                200: lib.response('Задача сохранена', definitions.SaveResponse),
                404: lib.response('Задача не найдена'),
                400: lib.response('Неправильные параметры запроса')
            }
        },
        delete: {
            summary: 'Удалить задачу',
            tags: ['tasks'],
            parameters: [
                lib.parameter('path', 'id', 'integer', {
                    description: 'ID задачи',
                    required: true
                })
            ],
            responses: {
                200: lib.response('Задача удалена'),
                404: lib.response('Задача не найдена')
            }
        }
    },
    '/tasks/{id}/check': {
        post: {
            tags: ['tasks'],
            summary: 'Завершить/возобновить задачу',
            operationId: 'checkTask',
            parameters: [
                lib.parameter('path', 'id', 'integer', {
                    description: 'ID задачи',
                    required: true
                })
            ],
            responses: {
                200: lib.response('Операция успешно выполнена'),
                404: lib.response('Задача не найдена')
            }
        }
    }
};
