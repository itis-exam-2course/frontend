const lib = require('./lib');
const _ = require('lodash');

/**
 * Swagger config
 */
const swagger = {
    swagger: '2.0',
    info: {
        title: 'Tasks React App',
        description: 'Документация по API для приложения управления задачами',
        version: '1.0.0'
    },
    host: 'localhost:5000',
    basePath: '/api',
    tags: [
        lib.tag('tasks', 'Задачи')
    ],
    definitions: require('./definitions'),
    paths: require('./paths'),
    'x-servers': [
        {url: 'http://localhost:5000/api', description: 'Локальное окружение'}
    ],
    schemes: ['http']
};

// генерация operationId
for (const pathKey in swagger.paths) {
    for (const methodKey in swagger.paths[pathKey]) {
        if (typeof swagger.paths[pathKey][methodKey].operationId === 'undefined') {
            let method = methodKey;
            let name = pathKey.replace('/', '_');
            let hasId = pathKey.indexOf('{id}') !== -1;

            if (method === 'post')
                method = hasId ? 'save' : 'create';
            else if (method === 'put')
                method = 'save';

            if (hasId || method === 'create') {
                name = _.trim(pathKey.replace(new RegExp('/', 'g'), '_').replace('{id}', ''), ' _');
                if (name[name.length - 1] === 's')
                    name = name.substr(0, name.length - 1);
            }

            swagger.paths[pathKey][methodKey].operationId = _.camelCase(method + '_' + name);
        }
    }
}

module.exports = swagger;
