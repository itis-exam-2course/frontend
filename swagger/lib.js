/**
 * Предзаготовленные конструкторы для объектов swagger
 * @type {{tag: function(*=, *=): {name: *, description: *}}}
 */
const lib = {
    /**
     * Тег
     * @param name {string} название
     * @param description {string} описание
     * @returns {{name: *, description: *}}
     */
    tag: (name, description) => ({name, description}),

    /**
     * Элемент объекта
     * @param type {string} тип
     * @param description {description} описание
     * @param def {string} значение по умолчанию
     * @returns {{type: *, description: *}}
     */
    property: (type, description, def) => (def ? {type, description, default: def} : {type, description}),

    /**
     * Параметр запрсоа
     * @param inType {string} in (query / path / body / ...)
     * @param name {string} название
     * @param type {string} тип параметра
     * @param extra {object} дополнительные параметры
     * @returns {{in: *, name: *, type: *}}
     */
    parameter: (inType, name, type, extra) => {
        const res = {
            in: inType,
            name
        };
        return typeof type === 'object' ? {...res, ...type} : {...res, type, ...extra};
    },

    postBody: definitionName => lib.parameter('body', definitionName, {schema: lib.definition(definitionName)}),

    /**
     * Ответ
     * @param description {string} описание
     * @param schema {object} схема
     * @returns {{description: *, schema: *}}
     */
    response: (description, schema) => {
        const res = {
            description,
            schema: {}
        };

        if (schema !== undefined && schema !== null && Object.keys(schema).length > 0)
            res.schema = schema;

        return res;
    },

    /**
     * Вызов definition
     * @param name
     * @returns {{$ref: string}}
     */
    definition: name => ({'$ref': '#/definitions/' + name})
};

module.exports = lib;
