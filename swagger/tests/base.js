import Ajv from 'ajv';
import assert from 'assert';
import mlog from 'mocha-logger';
import swagger from '../index';
import {init} from '../../app/utils/api';

export {swagger}

/**
 * Обрабатывает схему. Везде, где нет поля required, добавляет его таким образом, чтобы все поля были обязательными
 * @param schema схема
 * @returns {*} обработанная схема
 */
export function walkSchema(schema) {
    if (schema.type === 'object') {
        if (schema.required === undefined)
            schema.required = Object.keys(schema.properties);

        for (const key in schema.properties)
            schema.properties[key] = walkSchema(schema.properties[key]);
    }
    else if (schema.type === 'array' && schema.items !== undefined && schema.items.properties !== undefined)
        schema.items = walkSchema(schema.items);
    return schema;
}

/**
 * Проверяет ответ согласно json схеме
 * @param swaggerPath путь из документации сваггера (ключ из объекта paths)
 * @param method метод запроса
 * @param response ответ от сервера
 * @param statusCode=200 статус-код ответа
 */
export function validate(swaggerPath, method, response, statusCode = 200) {
    const swaggerResponse = swagger.paths[swaggerPath][method].responses;

    if (response instanceof Error)
        response = response.response;

    assert.equal(response.status, statusCode);

    const schema = walkSchema(swaggerResponse[statusCode].schema);

    const ajv = new Ajv();
    ajv.addFormat('date-time', /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/);

    const valid = ajv.validate(schema, response.data);

    let res = '';

    if (!valid) {
        res = ajv.errorsText();
        if (ajv.errors !== null)
            res += ':\n' + ajv.errors.map(x => `${x.dataPath.replace('.data.', '')} - ${x.message}`).join('\n')
    }
    assert(valid, res);
}

/**
 * Красиво логирует исключения запросов
 * @param e
 */
export function exceptionLogger(e) {
    let data = e.response.data;
    if (data.trace !== undefined)
        data.trace.length = 3
    mlog.log(`ERROR: ${e.request.method} ${e.request.path} - ${e.response.status}:\n`, JSON.stringify(data, true));
}

/**
 * Инициализация API-клиента
 */
export const api = init(`http://${(process.env.BACKEND_HOST || '127.0.0.1')}:${process.env.WEB_PORT || 5000}/api`);

require('axios-debug-log')({
    request(debug, config) {
        mlog.log('\t\tREQUEST: ' + config.url, JSON.stringify(config.data));
    },
    error(debug, error) {
        exceptionLogger(error);
        throw error;
    }
});
