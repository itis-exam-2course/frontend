import { api, validate } from './base'
import assert from 'assert'

/**
 * Работа с задачами
 */
describe('tasks', function () {
    let id;

    /**
     * Создание задачи
     */
    describe('create', () => {
        it('data error - 400', async () => {
            try {
                await api.createTask({'date': '2018-04-04 12:34'});
            }
            catch(e) {
                assert.equal(e.response.status, 400);
            }
        });

        it('ok - 200', async () => {
            const res = await api.createTask({title: '123', date: '2018-04-04T12:34:00'});
            console.log(res.data)
            id = res.data.id;
            validate('/tasks', 'post', res)
        });
    });

    /**
     * Получение задачи
     */
    describe('get', () => {
        it('not found - 404', async () => {
            try {
                const randomId = Math.floor(Math.random() * 10000);
                await api.getTask(randomId);
            }
            catch(e) {
                assert.equal(e.response.status, 404);
            }
        });
        it('ok - 200', async () => {
            const res = await api.getTask(id);
            validate('/tasks/{id}', 'get', res);
        })
    });

    /**
     * Обновление задачи
     */
    describe('update', () => {
        it('not found - 404', async () => {
            try {
                await api.saveTask(id * 1000000, {title: '1234', date: '2018-04-04T12:34:00'});
            }
            catch(e) {
                assert.equal(e.response.status, 404);
            }
        });

        it('data error - 400', async () => {
            try {
                await api.saveTask(id, {'date': '2018-04-04 12:34'});
            }
            catch(e) {
                assert.equal(e.response.status, 400);
            }
        });

        it('ok - 200', async () => {
            const res = await api.saveTask(id, {title: '1234', date: '2018-04-04T12:34:00'});
            validate('/tasks/{id}', 'put', res)
        });
    });

    /**
     * Изменение состояния задачи
     */
    describe('check', () => {
        it('not found - 404', async () => {
            try {
                await api.checkTask(id * 1000000);
            }
            catch(e) {
                assert.equal(e.response.status, 404);
            }
        });

        it('ok - 200', async () => {
            const res = await api.checkTask(id);
            validate('/tasks/{id}/check', 'post', res)
        });
    });

    /**
     * Удаление задачи
     */
    describe('delete', () => {
        it('not found - 404', async () => {
            try {
                await api.deleteTask(id * 1000000);
            }
            catch(e) {
                assert.equal(e.response.status, 404);
            }
        });

        it('ok - 200', async () => {
            const res = await api.deleteTask(id);
            validate('/tasks/{id}', 'delete', res)
        });
    });

    /**
     * Список
     */
    it('get list', () => api.getTasks().then(res => validate('/tasks', 'get', res)));
});
