const base = {
    id: {
        type: 'integer'
    },
    date: description => ({
        description,
        type: 'string',
        format: 'date-time',
        example: '2018-09-02T16:44:00'
    })
};

module.exports = base;
