const gulp = require('gulp');
const fs = require('fs');
const gulpSequence = require('gulp-sequence');
const express = require('express');
const httpProxy = require('http-proxy');

function createServer(isDebug = false) {
    const app = express();

    let port = null;

    app.use((req, res, next) => {
        if (req.query.port !== undefined)
            port = req.query.port;
        next();
    });

    if (isDebug)
        app.get('/', (req, res) => res.send(fs.readFileSync('./index.html').toString()));
    else
        app.use('/', express.static('public'));

    const proxy = httpProxy.createProxyServer();
    proxy.on('error', e => console.error(e));
    app.all('/api/*', (req, res) => {
        const target = `http://${process.env.BACKEND_HOST || '127.0.0.1'}:${port || process.env.WEB_PORT || 5000}`;
        console.log('api', target);
        return proxy.web(req, res, {target})
    });

    return app
}

module.exports = createServer;

gulp.task('spa-server', function() {
    const app = createServer();
    app.listen(3000);
    console.log('Server works on 3000');
});

gulp.task('server', gulpSequence(['default'], ['swagger-server', 'spa-server']));
