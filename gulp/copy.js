const gulp = require('gulp');

gulp.task('copy-html', () => gulp.src('./index.html').pipe(gulp.dest('./public')));

gulp.task('copy', ['copy-html']);
