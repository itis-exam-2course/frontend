const gulp = require('gulp');
const mocha = require('gulp-mocha');
const fs = require('fs');
const argv = require('minimist')(process.argv.slice(2));
const CodeGen = require('swagger-js-codegen').CodeGen;
const swagger = require('../../swagger/index');

/**
 * Генерирует JSON из swagger конфигурации
 */
gulp.task('swagger-json', () => fs.writeFileSync('swagger/swagger.json', JSON.stringify(swagger, null, '\t')));

gulp.task('swagger-server', ['swagger-json'], function() {
    const express = require('express');
    const app = express();
    app.get('/json', (req, res) => res.json(swagger));
    app.get('/js', (req, res) => res.send(fs.readFileSync('./node_modules/redoc/dist/redoc.min.js').toString()));

    app.get('/', (req, res) => res.send(
        `<!DOCTYPE html>
        <html>
          <head>
            <title>ReDoc</title>
            <meta charset="utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
          </head>
          <body>
            <redoc spec-url='/json' untrusted-spec lazy-rendering expand-responses="all"></redoc>
            <script src="/js"> </script>
          </body>
        </html>`
    ));
    const port = process.env.SWAGGER_SERVER_PORT !== undefined ? process.env.SWAGGER_SERVER_PORT : 8000;
    app.listen(port, '0.0.0.0');
    console.log('Swagger html server works on ' + port);
});

gulp.task('swagger-codegen-frontend', ['swagger-json'], () => {
    const code = CodeGen.getReactCode({
        className: 'Swagger',
        swagger: swagger,
        template: {
            method: fs.readFileSync('./gulp/swagger/method.mustache', 'utf-8')
        }
    });
    fs.writeFileSync('./app/utils/api/swagger.js', code);
});

gulp.task('swagger-test', () => {
    gulp.src(`./swagger/tests/${argv.only || '*'}`, {read: false}).pipe(mocha({
        timeout: 10000,
        require: ['babel-polyfill', 'mocha-clean', 'babel-register'],
        reporter: 'mochawesome',
        reporterOptions: {
            reporterFilename: 'swagger'
        },
        exit: true
    }))
});

gulp.task('swagger', ['swagger-codegen-frontend']);
