const gulp = require('gulp');
const mocha = require('gulp-mocha');
const argv = require('minimist')(process.argv.slice(2));

gulp.task('frontend-test', () => {
    return gulp.src(`./app/tests/${argv.only || '*'}`, {read: false})
        .pipe(mocha({
            timeout: 10000,
            require: ['babel-polyfill', 'mocha-clean', 'babel-register'],
            reporter: 'mochawesome',
            reporterOptions: {
                reporterFilename: 'frontend'
            },
            exit: true
        }))
        .once('error', () => process.exit(1))
        .once('end', () => process.exit())
});
