const gulp = require('gulp');
const webpack = require('webpack');
const path = require('path');
const gutil = require('gulp-util');
const parseArgs = require('minimist')(process.argv);
const env = parseArgs.e || 'development';

const rootDir = path.resolve(__dirname, '../'); // root js dir
const appDir = path.join(rootDir, 'app'); // dir to app
const outputDir = path.join(rootDir, 'public/dist');

const isDebug = env !== 'production';
// PLUGINS //
let plugins = [
    new webpack.DefinePlugin({
        __DEV__: isDebug,
        'process.env.NODE_ENV': JSON.stringify(env)
    })
];

const mainEntry = './index.js';

const config = {
    mode: env,
    context: appDir,
    devtool: isDebug ? 'cheap-inline-module-source-map' : false,
    entry: {
        app: mainEntry
    },
    output: {
        path: outputDir,
        filename: '[name].min.js',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [appDir, path.resolve(__dirname, '../node_modules')]
    },
    plugins: plugins,
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/, //new RegExp('node_modules\\' + path.sep + '(?!(react-dates|cleave.js)).*'),
                include: rootDir,
                use: [
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'env', 'stage-0'],
                            plugins: [
                                'transform-decorators-legacy',
                                'transform-runtime',
                                'transform-class-properties',
                                'transform-object-rest-spread'
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.less/,
                use: [
                    'isomorphic-style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: isDebug,
                            sourceMapContents: isDebug,
                            modules: true,
                            localIdentName: isDebug ? '[name]-[local]-[hash:base64:5]' : '[hash:base64:5]',
                            minimize: !isDebug,
                            discardComments: { removeAll: true }
                        }
                    },
                    {
                        loader: 'less-loader'
                    }
                ]
            },
            {
                test: /\.css/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    optimization: {
        splitChunks: {
            // include all types of chunks
            chunks: "all"
        }
    },
    stats: {
        assets: true,
        colors: true,
        errors: true,
        errorDetails: true,
        hash: true,
        modules: false,
        chunks: false,
        chunkModules: false,
        children: false,
        source: false
    }
};

const runner = webpack(config);

gulp.task('webpack', (callback) => runner.run((err, stats) => {
    if (err) throw new gutil.PluginError('webpack', err);

    gutil.log('[webpack]', stats.toString({
        colors: true,
        minimal: true,
        modules: false,
        warning: false
    }));

    callback();
}));

module.exports = runner;
module.exports.config = config;
