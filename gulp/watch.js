const gulp = require('gulp');
const notifier = require('node-notifier');
const WebpackOnBuildPlugin = require('on-build-webpack');
const webpack = require('webpack');
const webpackConfig = require('./webpack').config;
const createServer = require('./server');

gulp.task('watch', ['swagger-server'], function() {
    const app = createServer(true);

    let config = {
        ...webpackConfig,
        entry: {
            ...webpackConfig.entry,
            app: [
                webpackConfig.entry.app,
                'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr',
                'webpack/hot/dev-server'
            ]
        },
        plugins: [
            ...webpackConfig.plugins,
            new webpack.HotModuleReplacementPlugin(),
            new WebpackOnBuildPlugin(stats => notifier.notify('webpack build complete'))
        ]
    };

    const webpackRunner = webpack(config);

    app.use(require('webpack-dev-middleware')(webpackRunner, {
        publicPath: webpackConfig.output.publicPath,
        stats: {
            colors: true,
            minimal: true,
            chunks: false,
            modules: false
        }
    }));
    app.use(require('webpack-hot-middleware')(webpackRunner));

    app.listen(3000);

    console.log('Dev server works on 3000');
});
