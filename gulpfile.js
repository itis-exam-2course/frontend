const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');

require('./gulp/webpack');
require('./gulp/watch');
require('./gulp/copy');
require('./gulp/swagger');
require('./gulp/frontend-test');
require('./gulp/server');

gulp.task('default', gulpSequence(['swagger'], ['webpack', 'copy']));
