FROM node:8-alpine

RUN apk add git --no-cache

WORKDIR /app

COPY ./package.json .
COPY ./package-lock.json .

RUN npm i -q --max_old_space_size=512

COPY . .

CMD npm run swagger-server
