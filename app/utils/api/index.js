import axios from 'axios'
import serialize from 'jquery-param'
import swagger from './swagger';
import {trim} from 'lodash';
import {API_URL} from '../../constants/Urls'
import Fd from 'form-data';

/**
 * Исправленная реализация метода выполнения запросов через Swagger
 * @param method
 * @param url
 * @param parameters
 * @param body
 * @param headers
 * @param queryParameters
 * @param form
 * @param deferred
 */
swagger.Swagger.prototype.request = function(method, url, parameters, body, headers, queryParameters, form, deferred) {
    const queryParams = queryParameters && Object.keys(queryParameters).length ? serialize(queryParameters) : null;
    const urlWithParams = trim(url, '/') + (queryParams ? '?' + queryParams : '');

    let data = {};
    if (body !== null && body !== undefined && (Object.keys(body).length > 0 || body instanceof Fd))
        data = body;
    else if (form !== null && form !== undefined && (Object.keys(form).length > 0 || form instanceof Fd))
        data = form;
    else
        data = parameters;

    let requestParams = {
        method: method.toLowerCase(),
        url: urlWithParams,
        data,
        timeout: 10000,
    };

    const req = axios(requestParams);
    return deferred === undefined
        ? req
        : req.then(body => deferred.resolve(body)).catch(error => deferred.reject(error));
};

export function init(host) {
    return new swagger.Swagger({domain: host});
}

const api = init(API_URL);

export default api;
