import axios from 'axios';
import {API_URL} from '../constants/Urls';
import serialize from 'jquery-param'

const client = axios.create({
    baseURL: API_URL,
    responseType: 'json',
    withCredentials: true,
    paramsSerializer: params => serialize(params)
});

export default client;
