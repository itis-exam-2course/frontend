import Immutable from 'immutable';
import faker from 'faker';
import chai, {expect} from 'chai';
import chaiImmutable from 'chai-immutable';
import reducer, {initialState} from '../../reducers/tasks';
import {
    checkTaskSuccess,
    deleteTaskSuccess,
    getTasksSuccess,
    saveTaskSuccess
} from "../../actions/TasksActions";

chai.use(chaiImmutable);

const generateFakeItem = () => ({
    id: faker.random.uuid(),
    title: faker.lorem.word(),
    date: "2018-09-02T16:44:00",
    isDone: faker.random.boolean()
});

describe('list', () => {
    const item = generateFakeItem();
    const getAction = getTasksSuccess({data: [item]});

    it('get - should put data into state', () => {
        const stateAfterGet = Immutable.fromJS({list: [item]});
        expect(reducer(undefined, getAction)).equal(stateAfterGet)
    });
});

describe('check task', () => {
    it('should change isDone flag', () => {
        const item = generateFakeItem();
        const before = Immutable.fromJS({list: [{...item, isDone: false}]});
        const afterOne = Immutable.fromJS({list: [{...item, isDone: true}]});

        expect(reducer(before, checkTaskSuccess(item.id))).equal(afterOne);
        expect(reducer(afterOne, checkTaskSuccess(item.id))).equal(before);
    });
});

describe('delete task', () => {
    it('should delete task from list', () => {
        const item = generateFakeItem();
        const before = Immutable.fromJS({list: [item]});
        expect(reducer(before, deleteTaskSuccess(item.id))).equal(initialState);
    });
});

describe('save task', () => {
    it('should add new task in list', () => {
        const item = generateFakeItem();
        const after = Immutable.fromJS({list: [item]});
        expect(reducer(undefined, saveTaskSuccess(item))).equal(after);
    });
    it('should update task in list', () => {
        const item = generateFakeItem();
        const before = Immutable.fromJS({list: [item]});
        const newItem = {...item, title: '123333333'};
        const after = Immutable.fromJS({list: [newItem]});
        expect(reducer(before, saveTaskSuccess(newItem))).equal(after);
    });
});
