import React from 'react';
import {ImmutableLoadingBar} from 'react-redux-loading-bar'

export default class LB extends React.Component {
    render() {
        return <ImmutableLoadingBar style={{
            backgroundColor: '#007bff',
            zIndex: 10000,
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%'
        }} />;
    }
}
