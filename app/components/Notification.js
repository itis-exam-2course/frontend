import React from 'react';
import PropTypes from 'prop-types';
import {Alert} from "reactstrap";

export default class Notification extends React.Component {
    static propTypes = {
        type: PropTypes.string,
        children: PropTypes.string.isRequired,
    };
    render() {
        const { type, children } = this.props;
        return (
            <Alert color={type === 'error' ? 'danger' : type}>{children}</Alert>
        );
    }
}
