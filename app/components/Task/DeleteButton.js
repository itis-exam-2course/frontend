import React from 'react';
import {Button} from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faTrash from '@fortawesome/fontawesome-free-solid/faTrash'

export default class CheckButton extends React.Component {
    render() {
        return <Button color='danger' size='sm' {...this.props}><FontAwesomeIcon icon={faTrash}/></Button>;
    }
}
