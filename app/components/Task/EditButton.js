import React from 'react';
import {Button} from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit'

export default class EditButton extends React.Component {
    render() {
        return (
            <Button color='primary' size='sm' {...this.props}>
                <FontAwesomeIcon icon={faEdit}/>
            </Button>
        );
    }
}
