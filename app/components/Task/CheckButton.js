import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck'
import faSquare from '@fortawesome/fontawesome-free-regular/faSquare'

export default class CheckButton extends React.Component {
    static propTypes = {
        completed: PropTypes.boolean
    };

    render() {
        const {completed} = this.props;
        return (
            <Button color={completed ? 'success' : 'primary'} size='sm' {...this.props}>
                <FontAwesomeIcon icon={completed ? faCheck : faSquare}/>
            </Button>
        );
    }
}
