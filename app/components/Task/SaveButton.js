import React from 'react';
import {Button} from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faSave from '@fortawesome/fontawesome-free-solid/faSave'

export default class SaveButton extends React.Component {
    render() {
        return (
            <Button color='primary' size='sm' {...this.props}>
                <FontAwesomeIcon icon={faSave}/>
            </Button>
        );
    }
}
