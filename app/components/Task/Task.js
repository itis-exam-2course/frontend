import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {ButtonGroup, Col, ListGroupItem, Row} from "reactstrap";
import CheckButton from "./CheckButton";
import DeleteButton from "./DeleteButton";
import TaskContent from "./TaskContent";
import EditButton from "./EditButton";

export default class Task extends React.Component {
    static propTypes = {
        check: PropTypes.func,
        edit: PropTypes.func,
        del: PropTypes.func,
        item: PropTypes.object
    };

    render() {
        const {check, edit, item, del} = this.props;
        return (
            <ListGroupItem>
                <Row>
                    <Col sm={1}>
                        <CheckButton completed={item.isDone} onClick={check}/>
                    </Col>
                    <Col sm={10}>
                        <TaskContent date={moment(item.date).format('DD.MM.YYYY HH:mm')}>{item.title}</TaskContent>
                    </Col>
                    <Col sm={1}>
                        <ButtonGroup>
                            <EditButton onClick={edit}/>
                            <DeleteButton onClick={del}/>
                        </ButtonGroup>
                    </Col>
                </Row>
            </ListGroupItem>
        );
    }
}


