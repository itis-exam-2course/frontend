import React from 'react';
import PropTypes from 'prop-types';

export default class TaskContent extends React.Component {
    static propTypes = {
        children: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired
    };
    render() {
        return (
            <div>
                <div>{this.props.children}</div>
                <small>{this.props.date}</small>
            </div>
        );
    }
}
