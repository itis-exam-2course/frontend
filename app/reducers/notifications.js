import Immutable from 'immutable';
import { NEW_NOTIFICATION, REMOVE_NOTIFICATION } from '../constants/ActionTypes'

const initialState = Immutable.fromJS([]);

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case NEW_NOTIFICATION:
            return state.push(action.payload);
        case REMOVE_NOTIFICATION:
            return state.filter(x => x.id !== action.payload);
        default:
            return state;
    }
}
