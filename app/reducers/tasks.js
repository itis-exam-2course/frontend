import Immutable from 'immutable';
import {
    CHECK_TASK_SUCCESS,
    DELETE_TASK_SUCCESS,
    GET_TASKS_SUCCESS,
    SAVE_TASK_SUCCESS
} from "../constants/ActionTypes";

export const initialState = Immutable.fromJS({
    list: []
});

/**
 * Обновление одной записи в списке
 * @param state состояние
 * @param updatedEntity обновленная сущность
 * @returns {*}
 */
function updateEntity(state, updatedEntity) {
    let path = ['list'];
    let list = state.getIn(path);

    /**
     * Функция проверки элементов при поиске
     * @param record
     * @returns {boolean} подходит ли нам элемент
     */
    const predicate = record => {
        if (updatedEntity.oldId === null) // новая запись
            return record.get('id') === '';

        return updatedEntity.id === record.get('id');
    };

    // поиск индекса
    const index = list.findIndex(predicate);
    // вставка значения
    if (index === -1) // новая запись
        return state.updateIn(path, s => s.push(Immutable.fromJS(updatedEntity)));
    path.push(index);

    // для существующей записи
    Object.keys(updatedEntity).forEach(key => {
        const check = key !== 'id' && updatedEntity[key] !== null && updatedEntity[key] !== undefined;

        if (check)
            state = state.updateIn(path, stateEntity => stateEntity.set(key, updatedEntity[key]))
    });

    return state;
}

export default function tasks(state = initialState, action) {
    let listKeyPath = ['list'];

    switch (action.type) {
        case GET_TASKS_SUCCESS:
            return state.setIn(listKeyPath, Immutable.fromJS(action.payload.data));

        // удачное сохранение записи
        case SAVE_TASK_SUCCESS:
            return action.payload.entity.id === undefined ? state : updateEntity(state, action.payload.entity);

        case CHECK_TASK_SUCCESS:
            const index = state.get('list').findIndex(x => x.get('id') === action.payload.id);
            const path = ['list', index, 'isDone'];
            const current = state.getIn(path);
            return state.updateIn(path, s => !current);

        // удаление одного элемента - получаем единственное число в названии сущности
        case DELETE_TASK_SUCCESS:
            return state.updateIn(listKeyPath, s => s.filter(x => x.get('id') !== action.payload.id));

        default:
            return state;
    }
}
