import Immutable from 'immutable';
import { combineReducers } from 'redux-immutablejs';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar'
import { LOCATION_CHANGE } from 'react-router-redux';
import tasks from './tasks'
import notifications from './notifications'

export default combineReducers({
    tasks,
    loadingBar,
    notifications,
    routing(state = Immutable.fromJS({previousLocation: null, locationBeforeTransitions: null}), action) {
        if (action.type === LOCATION_CHANGE) {
            const beforeTransitions = state.get('locationBeforeTransitions');
            if (
                beforeTransitions === null ||
                (beforeTransitions !== null && beforeTransitions.toJS() !== action.payload)
            ) {
                return state.merge({
                    previousLocation: beforeTransitions !== null ? beforeTransitions.toJS() : null,
                    locationBeforeTransitions: action.payload
                });
            }
        }
        return state;
    }
});
