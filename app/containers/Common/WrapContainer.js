import React from 'react';
import PropTypes from 'prop-types';
import {Navbar, NavbarBrand, Container} from "reactstrap";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import GitIcon from '@fortawesome/fontawesome-free-brands/faGit'
import LoadingBar from "../../components/LoadingBar";
import NotificationsContainer from "./NotificationsContainer";

export default class WrapContainer extends React.Component {
    static propTypes = {
        children: PropTypes.element.isRequired
    };

    render() {
        const {children} = this.props;
        return (
            <div>
                <LoadingBar />
                <Navbar color="dark" dark expand="md">
                    <Container>
                        <NavbarBrand href="/">Issues React App</NavbarBrand>
                    </Container>
                </Navbar>
                <Container>
                    <br/>
                    {children}
                    <hr/>
                    <footer>
                        &copy; 2018 <a href="http://kpfu.ru/itis" target='_blank'>КФУ ИТИС</a> | <a href="https://gitlab.com/itis-exam-2course" target='_blank'>
                            <FontAwesomeIcon icon={GitIcon} /> Исходный код
                        </a>
                    </footer>
                </Container>
                <NotificationsContainer />
            </div>
        );
    }
}
