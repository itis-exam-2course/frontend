import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import Notification from '../../components/Notification'

const styles = {
    maxWidth: 300,
    position: 'fixed',
    left: 5,
    bottom: 5
};

@connect((state) => ({notifications: state.get('notifications').toJS()}))
export default class NotificationsContainer extends React.Component {
    static propTypes = {
        notifications: PropTypes.array
    };
    render() {
        const { notifications } = this.props;
        return (
            <div style={styles}>
                {notifications.map((elem, key) => <Notification type={elem.type} key={key}>{elem.text}</Notification>)}
            </div>
        );
    }
}
