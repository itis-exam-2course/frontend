import React from 'react';
import PropTypes from 'prop-types';

export default class ErrorContainer extends React.Component {
    static propTypes = {
        code: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        text: PropTypes.string
    };
    static defaultProps = {
        code: 404,
        text: 'Страница не найдена'
    };
    render() {
        return <div title={this.props.code} text={this.props.text} />;
    }
}
