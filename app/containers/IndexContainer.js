import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {ListGroup} from "reactstrap";
import {checkTask, deleteTask, getTasks} from "../actions/TasksActions";
import FormController from "./FormController";
import Task from "../components/Task/Task";

@connect(state => ({tasks: state.getIn(['tasks', 'list']).toJS()}), {getTasks, checkTask, deleteTask})
export default class IndexContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editingId: null
        }
    }

    static propTypes = {
        tasks: PropTypes.array,
        getTasks: PropTypes.func,
        checkTask: PropTypes.func,
        deleteTask: PropTypes.func
    };

    componentDidMount() {
        this.props.getTasks();
    }

    render() {
        const {checkTask, deleteTask} = this.props;
        const {editingId} = this.state;
        return (
            <div>
                <ListGroup>
                    {this.props.tasks.map((item, key) =>
                        editingId === item.id
                            ? <FormController key={key} data={item} afterSave={() => this.setState({editingId: null})} />
                            : <Task
                                key={key}
                                item={item}
                                edit={() => this.setState({editingId: item.id})}
                                del={() => deleteTask(item.id)}
                                check={() => checkTask(item.id)}
                            />
                    )}
                    <FormController titlePlaceholder='Создать новую задачу...'/>
                </ListGroup>
            </div>
        );
    }
}
