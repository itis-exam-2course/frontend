import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import moment from "moment";
import {Col, Input, InputGroup, InputGroupAddon, ListGroupItem, Row} from "reactstrap";
import CheckButton from "../components/Task/CheckButton";
import SaveButton from "../components/Task/SaveButton";
import {createTask, getTasks, saveTask} from "../actions/TasksActions";

const initialState = {
    id: null,
    title: '',
    date: moment().add(1, 'day').format('YYYY-MM-DDTHH:mm')
};

@connect(state => ({}), {getTasks, saveTask, createTask})
export default class FormController extends React.Component {
    constructor(props) {
        super(props);
        const extra = props.data || {};
        this.state = {...initialState, ...extra}
    }

    static propTypes = {
        data: PropTypes.object,
        titlePlaceholder: PropTypes.string,
        createTask: PropTypes.func,
        saveTask: PropTypes.func,
        getTasks: PropTypes.func,
        afterSave: PropTypes.func
    };
    static defaultProps = {
        titlePlaceholder: 'Название задачи'
    };

    change(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    async save(e) {
        e.preventDefault();
        if (this.state.title === '' || this.state.title === null) {
            alert('Введите название задачи');
            return;
        }
        if (this.state.id) {
            await this.props.saveTask(this.state);
        }
        else {
            const {title, date} = this.state;
            await this.props.createTask({title, date});
            this.setState({...initialState});
            this.props.getTasks();
        }
        if (this.props.afterSave !== undefined)
            this.props.afterSave();
    }

    render() {
        const {titlePlaceholder} = this.props;
        const {title, date} = this.state;
        return (
            <ListGroupItem>
                <form action='' onSubmit={e => this.save(e)}>
                    <Row>
                        <Col sm={1}>
                            <CheckButton disabled completed={this.props.data && this.props.data.isDone || false}/>
                        </Col>
                        <Col sm={10}>
                            <InputGroup>
                                <Input placeholder={titlePlaceholder} size='sm' type='text' name='title' id='title'
                                       value={title}
                                       onChange={e => this.change(e)}/>
                                <InputGroupAddon addonType='append'>
                                    <Input placeholder='Срок' size='sm' type='datetime-local' name='date' id='date'
                                           value={date}
                                           onChange={e => this.change(e)}/>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                        <Col sm={1}>
                            <SaveButton/>
                        </Col>
                    </Row>
                </form>
            </ListGroupItem>
        );
    }
}
