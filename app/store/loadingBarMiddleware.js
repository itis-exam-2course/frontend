import { hideLoading, showLoading } from 'react-redux-loading-bar'

/**
 * Реализует показываение и скрытие индикатора загрузки во время выполнения сетевых запросов
 * @param store
 * @returns {function(*): function(*=)}
 */
const loadingBarMiddleware = store => next => action => {
    const type = action.type;
    if (type.indexOf('GET_') !== -1 || type.indexOf('SAVE_') !== -1 || type.indexOf('CREATE_') !== -1 ||
        type.indexOf('DELETE_') !== -1 || type.indexOf('CHECK_') !== -1) {
        if (action.type.indexOf('_SUCCESS') !== -1 || action.type.indexOf('_FAIL') !== -1)
            store.dispatch(hideLoading());
        else if (!store.loadingBar)
            store.dispatch(showLoading());
    }
    return next(action)
};
export default loadingBarMiddleware;
