import {notification} from '../actions/NotificationsActions';

/**
 * Показывает сообщение об ошибке, если сетевой запрос завершился неудачно
 * @param store
 * @returns {function(*): function(*=)}
 */
const notificationsMiddleware = store => next => action => {
    if (action.type.indexOf('GET_') !== -1 && action.type.indexOf('_FAIL') !== -1) {
        if (action.type.indexOf('_ONE') !== -1) {
            store.dispatch(notification(
                `Произошла ошибка при получении ${action.type.replace('GET_', '').replace('_ONE_FAIL', '')}`,
                'error'
            ));
        }
        else {
            store.dispatch(notification(
                `Произошла ошибка при получении списка ${action.type.replace('GET_', '').replace('_FAIL', '')}`,
                'error'
            ));
        }
    }
    return next(action);
};

export default notificationsMiddleware;
