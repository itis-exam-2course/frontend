import {createStore, compose, applyMiddleware} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from '../reducers';
import loadingBarMiddleware from "./loadingBarMiddleware";
import notificationsMiddleware from "./notificationsMiddleware";

/**
 * Создает Redux store
 * @param history
 * @returns {Store<any> | *}
 */
export default function configureStore(history) {
    const middleware = applyMiddleware(
        routerMiddleware(history),
        createLogger({
            collapsed: true,
            duration: true
        }),
        thunk,
        loadingBarMiddleware,
        notificationsMiddleware
    );

    const enhancers = [];
    if (__DEV__) {
        localStorage.debug = 'app:*';
        const devToolsExtension = window.devToolsExtension;
        if (typeof devToolsExtension === 'function')
            enhancers.push(devToolsExtension())
    }

    let createStoreWithMiddleware = compose(
        middleware,
        ...enhancers
    );

    let store = createStoreWithMiddleware(createStore)(rootReducer);

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const reducers = require('../reducers/index').default;
            store.replaceReducer(reducers);
        });
    }
    return store;
}
