import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux'
import {Router} from 'react-router/es';
import 'bootstrap/dist/css/bootstrap.min.css'

/**
 * Основной компонент приложения
 */
export default class App extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        routes: PropTypes.object.isRequired,
        routerKey: PropTypes.number
    };

    static childContextTypes = {
        insertCss: PropTypes.func.isRequired
    };

    getChildContext() {
        return {
            insertCss: styles => styles._insertCss()
        }
    }

    render() {
        const { history, routes, routerKey, store } = this.props;
        return (
            <Provider store={store}>
                <Router
                    history={history}
                    children={routes}
                    key={routerKey}
                />
            </Provider>
        );
    }
}
