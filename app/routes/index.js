import React from 'react';
import IndexContainer from "../containers/IndexContainer";
import WrapContainer from "../containers/Common/WrapContainer";
import ErrorContainer from "../containers/ErrorContainer";

/**
 * Создает роуты для приложения из конфгурации
 * @param store
 * @returns {{childRoutes: *}}
 */
export const createRoutes = (store) => ({
    childRoutes: [
        {
            path: '/',
            component: WrapContainer,
            indexRoute: {
                component: IndexContainer
            },
            childRoutes: [
                // {
                //     path: 'login',
                //     component: p.LoginContainer
                // }
            ]
        },
        {
            path: '*',
            component: ErrorContainer
        }
    ]
});

export default createRoutes;
