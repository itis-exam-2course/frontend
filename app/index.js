import React from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import configureStore from './store/configureStore';
import AppContainer from './app';

const store = configureStore(browserHistory);
const history = syncHistoryWithStore(browserHistory, store, {
    selectLocationState (state) {
        return state.get('routing').toJS();
    }
});
const MOUNT_NODE = document.getElementById('root');

let render = (routerKey = null) => {
    const routes = require('./routes').default(store);
    ReactDOM.render(<AppContainer store={store} history={history} routes={routes} routerKey={routerKey} />, MOUNT_NODE);
    if (document.getElementById('loading'))
        document.getElementById('loading').remove()
};

if (__DEV__ && module.hot) {
    console.log('HOT');
    const renderApp = render;
    const renderError = (error) => {
        const RedBox = require('redbox-react').default;
        ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    };
    render = () => {
        try {
            renderApp(Math.random())
        } catch (error) {
            renderError(error)
        }
    };
    module.hot.accept(['./routes'], () => render())
}

render();
