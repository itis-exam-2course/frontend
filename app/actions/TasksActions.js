import api from '../utils/api'
import {
    CHECK_TASK, CHECK_TASK_FAIL, CHECK_TASK_SUCCESS,
    DELETE_TASK, DELETE_TASK_FAIL, DELETE_TASK_SUCCESS,
    GET_TASKS, GET_TASKS_FAIL, GET_TASKS_SUCCESS, SAVE_TASK, SAVE_TASK_FAIL, SAVE_TASK_SUCCESS
} from "../constants/ActionTypes";

export function getTasksSuccess(payload) {
    return {type: GET_TASKS_SUCCESS, payload}
}

export function getTasks() {
    return dispatch => {
        dispatch({type: GET_TASKS});
        return api.getTasks().then(
            payload => dispatch(getTasksSuccess(payload)),
            payload => {
                dispatch({type: GET_TASKS_FAIL, payload});
                throw payload;
            }
        );
    }
}

export function checkTaskSuccess(id, payload) {
    return {type: CHECK_TASK_SUCCESS, payload: {...payload, id}};
}

export function checkTask(id) {
    return dispatch => {
        dispatch({type: CHECK_TASK, payload: {id}});
        return api.checkTask(id).then(
            payload => dispatch(checkTaskSuccess(id, payload)),
            payload => {
                dispatch({type: CHECK_TASK_FAIL, payload});
                throw payload;
            }
        );
    }
}

export function deleteTaskSuccess(id, payload) {
    return {type: DELETE_TASK_SUCCESS, payload: {...payload, id}};
}

export function deleteTask(id) {
    return dispatch => {
        dispatch({type: DELETE_TASK, payload: {id}});
        return api.deleteTask(id).then(
            payload => dispatch(deleteTaskSuccess(id, payload)),
            payload => {
                dispatch({type: DELETE_TASK_FAIL, payload});
                throw payload;
            }
        );
    }
}

export function saveTaskSuccess(entity, payload) {
    return {type: SAVE_TASK_SUCCESS, payload: {...payload, entity}};
}

export function createTask(entity) {
    return dispatch => {
        dispatch({type: SAVE_TASK, payload: {entity}});
        return api.createTask(entity).then(
            payload => dispatch(saveTaskSuccess(entity, payload)),
            payload => {
                dispatch({type: SAVE_TASK_FAIL, payload});
                throw payload;
            }
        );
    }
}

export function saveTask(entity) {
    return dispatch => {
        dispatch({type: SAVE_TASK, payload: {entity}});
        return api.saveTask(entity.id, entity).then(
            payload => dispatch(saveTaskSuccess(entity, payload)),
            payload => {
                dispatch({type: SAVE_TASK_FAIL, payload});
                throw payload;
            }
        );
    }
}