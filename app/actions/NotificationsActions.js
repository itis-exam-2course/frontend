import { NEW_NOTIFICATION, REMOVE_NOTIFICATION } from '../constants/ActionTypes'

/**
 * Создание уведомления
 * @param text текст
 * @param type тип (success, warning, error, info)
 * @param timeout время показа уведомления в секундах
 * @returns {Function}
 */
export function notification(text, type = 'success', timeout = 5) {
    return dispatch => {
        const id = (new Date()).getTime();
        dispatch({
            type: NEW_NOTIFICATION,
            payload: {id, type, text}
        });
        setTimeout(() => dispatch(removeNotification(id)), timeout * 1000);
    }
}

export function removeNotification(id) {
    return {
        type: REMOVE_NOTIFICATION,
        payload: id
    };
}
