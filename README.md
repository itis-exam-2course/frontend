# Одностраничное приложение для экзамена по информатике для C#-потока 2 курса ИТИС, 2018 год

## Процесс запуска приложения

_Составлен в основном для пользователей на операционной системе Windows. Для Linux/MacOS все операции выполняются проще. Тестировалось на Windows 7 с git bash, cmd, Google Chrome 66._

1. [Скачать и установить NodeJS 8 версии](https://nodejs.org/en/)
2. Открыть консоль (`Выполнить / cmd` либо `Git Bash`). Если консоль была открыта ранее, ее нужно перезапустить
3. Выполнить команды: 
    ```
    npm -v 
    node -v
    ```
    В ответ должны быть напечатаны версии `npm` и `node`. Если этого не произошло, скорее всего проблема в переменной окружения PATH. Нужно дописать в эту переменную путь `C:\Program Files\nodejs`. [Подробная инструкция по установке переменных окружения](https://www.java.com/ru/download/help/path.xml).
4. Склонировать проект
5. Открыть консоль в папке проекта
    - правый клик по папке, `git bash`
    - Выполнить `cd your/awesome/project/dir`
6. Выполнить `npm install`. Произойдет установка модулей nodejs
7. Выполнить `npm run server`. Дождаться окончания сборки, которое знаменуется фразой `Server works on 3000`

- [localhost:3000](http://localhost:3000) - по этому адресу будет запущено **одностраничное приложение**
- [localhost:8000](http://localhost:8000) - по этому адресу будет запущен **сервер API-документации** 

## Задача

Разработать серверную часть в виде ASP.NET Core Web API для приложения по управлению задачами согласно [API-документации](http://localhost:8000).

По умолчанию приложение пытается подключиться к серверной части по адресу `http://localhost:5000`. 

При необходимости при запуске фронтенда можно измененить порт сервера в адресной строке, например: `http://localhost:3000/?port=1234`

Во время работы сервера веб-приложения, в консоли выводится строчка `api http://myhost:2000`, которая сообщает о том, к какому бекенду будет подключаться веб-приложение в процессе работы.

Во время разработки открывайте консоль разработчика в браузере (CTRL+SHIFT+I), смотрите на выводимые в консоль (Console) ошибки и на лог сетевых запросов (Network).

Во время разработки в классе для принятия данных поля должны быть написаны с большой буквы, однако фронтенд будет отправлять поля с маленькой буквы. ASP.NET Core автоматически преобразовывает регистр полей при принятии данных.

### Альтернативные способы изменения порта

**git bash:**

```bash
BACKEND_HOST=myhost WEB_PORT=2000 npm run server
```

**cmd:**

```bash
set BACKEND_HOST=myhost
set WEB_PORT=2000
npm run server
```

где `myhost` - это адрес серверной части, а `2000` - это порт серверной части.

## Тесты

Для проверки работы серверной части можно запускать API-тесты:

- `npm run swagger-test` - запустить тесты на серверную часть
- `BACKEND_HOST=myhost WEB_PORT=2000 npm run swagger-test` - запуск тестов с указанием альтернативных адресов серверной части (подробнее выше)
- `npm run frontend-test` - запуск тестов фронтенда

## Возможные ошибки в системной консоли

- `Error: connect ECONNREFUSED 127.0.0.1:5000` - не удалось подключиться к серверной части по указанному адресу. Нужно убедиться, что бекенд работает корректно, и он слушает именно такой адрес.
- `EADDRINUSE :::3000` - порт 3000 занят. Скорее всего, уже запущен процесс сервера веб-приложения `node`, и его нужно остановить через "Диспетчер задач"" (вкладка "Процессы"), перед запуском нового. 

## Технологии

- **Сборка:**
    - [NodeJS 8](https://nodejs.org/),
    - [webpack 4](https://webpack.js.org/),
    - [gulp](https://gulpjs.com/),
    - [babel](https://babeljs.io/)
- **Сервер веб-приложения:**
    - [express](http://expressjs.com/)
- **Веб-приложение**: 
    - [react 16.4](https://reactjs.org/) - отображение,
    - [redux 4](https://redux.js.org/) - управление состоянием,
    - [react-router 3](https://github.com/ReactTraining/react-router) - роутинг,
    - [redux-thunk](https://github.com/reduxjs/redux-thunk) - ассинхронные операции,
    - [immutable.js](https://facebook.github.io/immutable-js/docs/) - неизменяемое состояние,
    - [axios](https://github.com/axios/axios) - сетевые запросы,
    - [moment](http://momentjs.com/) - работа с датой
- **Дизайн:**
    - [reactstrap](https://reactstrap.github.io/) - [Bootstrap 4](https://getbootstrap.com/) component library,
    - [FontAwesome 5](https://fontawesome.com/) - иконки
- **Документация API:**
    - [swagger](https://swagger.io/) - документация,
    - [swagger-js-codegen](https://github.com/atnartur/swagger-js-codegen) - генератор кода отправки сетевых запросов,
    - [ReDoc](https://github.com/Rebilly/ReDoc) - просмотр API-документаци
- **Автотесты:**
    - [mocha](https://mochajs.org/) - запуск,
    - [chai](http://www.chaijs.com/) - assertion library,
    - [ajv](https://github.com/epoberezkin/ajv) - проверка JSON схемы,
    - [faker](https://github.com/marak/Faker.js/) - генератор тестовых данных 
